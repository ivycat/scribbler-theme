<?php
/**
 * @package Scribbler
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="entry-content row">
		<div class="col-xs-4">
			<?php if ( has_post_thumbnail() ) {
				the_post_thumbnail();
			} ?>
			<div>
				<h4>Other Information</h4>
				<?php wordslinger_book_meta_display(); ?>
				<hr />
				<?php wordslinger_book_buy_link_display(); ?>
			</div>
		</div>
		<div class="col-md-8">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->
			<?php the_content(); ?>
		</div>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'scribbler' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php scribbler_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
