<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Scribbler
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'scribbler' ); ?></a>
	
	<header class="banner-top">
		<div class="container">
			<?php wp_nav_menu( array( 
				'theme_location' => 'social',
				'container'       => false,
				'menu_class'      => 'nav navbar-nav pull-right',//  navbar-right
				'walker'          => new Bootstrap_Nav_Menu(),
			 ) ); ?>
		</div>
	</header>

	<header id="masthead" class="site-header container" role="banner">
		<div class="row">
			<div class="site-branding col-md-4">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
			</div>

			<nav id="site-navigation" class="col-md-8 main-navigation" role="navigation">
				<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', '_s' ); ?></a>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-main">
						<span class="sr-only"><?php _e('Toggle navigation', '_s'); ?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!--<a class="navbar-brand" href="#">Brand</a>-->
				</div>

				<div class="collapse navbar-collapse" id="navbar-collapse-main">
					<?php
					wp_nav_menu( array(
							'theme_location'  => 'primary',
							'container'       => false,
							'menu_class'      => 'nav navbar-nav',//  navbar-right
							'walker'          => new Bootstrap_Nav_Menu(),
						)
					);
					// get_search_form();
					?>
				</div><!-- /.navbar-collapse -->

			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content container">
