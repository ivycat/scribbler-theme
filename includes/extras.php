<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Scribbler
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function scribbler_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'scribbler_body_classes' );

if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
	/**
	 * Filters wp_title to print a neat <title> tag based on what is being viewed.
	 *
	 * @param string $title Default title text for current view.
	 * @param string $sep Optional separator.
	 * @return string The filtered title.
	 */
	function scribbler_wp_title( $title, $sep ) {
		if ( is_feed() ) {
			return $title;
		}

		global $page, $paged;

		// Add the blog name
		$title .= get_bloginfo( 'name', 'display' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $sep $site_description";
		}

		// Add a page number if necessary:
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title .= " $sep " . sprintf( esc_html__( 'Page %s', 'scribbler' ), max( $paged, $page ) );
		}

		return $title;
	}
	add_filter( 'wp_title', 'scribbler_wp_title', 10, 2 );

	/**
	 * Title shim for sites older than WordPress 4.1.
	 *
	 * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function scribbler_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}
	add_action( 'wp_head', 'scribbler_render_title' );
endif;

/**
 * Elegantly add Google Fonts to theme
 * Reference: 
 * http://themeshaper.com/2014/08/13/how-to-add-google-fonts-to-wordpress-themes/
 */
function scribbler_theme_slug_fonts_url() {
	$fonts_url = '';
 
	/* Translators: If there are characters in your language that are not
	* supported by Playfair Display, translate this to 'off'. Do not translate
	* into your own language.
	*/
	$playfair = _x( 'on', 'Playfair Display font: on or off', 'theme-slug' );
 
	/* Translators: If there are characters in your language that are not
	* supported by Open Sans, translate this to 'off'. Do not translate
	* into your own language.
	*/
	$open_sans = _x( 'on', 'Open Sans font: on or off', 'theme-slug' );
 
	if ( 'off' !== $playfair || 'off' !== $open_sans ) {
		$font_families = array();
 
		if ( 'off' !== $playfair ) {
			$font_families[] = 'Playfair Display:400,700,400italic';
		}
 
		if ( 'off' !== $open_sans ) {
			$font_families[] = 'Open Sans:700italic,400,800,600';
		}
 
		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);
 
		$fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
	}
 
	return esc_url_raw( $fonts_url );
}

function theme_slug_scripts_styles() {
	wp_enqueue_style( 'theme-slug-fonts', scribbler_theme_slug_fonts_url(), array(), null );
}
add_action( 'wp_enqueue_scripts', 'theme_slug_scripts_styles' );
