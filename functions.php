<?php

/**
 * Scribbler functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package Scribbler
 * @since 0.1.0
 */

// Useful global constants
define( 'SCRIBBLER_VERSION',      '0.1.1' );
define( 'SCRIBBLER_URL',          get_stylesheet_directory_uri() );
define( 'SCRIBBLER_TEMPLATE_URL', get_template_directory_uri() );
define( 'SCRIBBLER_PATH',         get_template_directory() . '/' );
define( 'SCRIBBLER_INC',          SCRIBBLER_PATH . 'includes/' );

// Include compartmentalized functions
require_once SCRIBBLER_INC . 'functions/core.php';

// Include lib classes

if ( ! function_exists( 'scribbler_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function scribbler_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Scribbler, use a find and replace
	 * to change 'scribbler' to the name of your theme in all the template files
	 */
	// load_theme_textdomain( 'scribbler', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'scribbler' ),
		'social' => esc_html__( 'Social Menu', 'scribbler' ),
		'footer' => esc_html__( 'Footer Menu', 'scribbler' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'scribbler_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // scribbler_setup
add_action( 'after_setup_theme', 'scribbler_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function scribbler_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'scribbler_content_width', 640 );
}
add_action( 'after_setup_theme', 'scribbler_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function scribbler_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'scribbler' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer - Left', 'scribbler' ),
		'id'            => 'footer-1',
		'description'   => 'Footer - Left',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer - Middle', 'scribbler' ),
		'id'            => 'footer-2',
		'description'   => 'Footer - Middle',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer - Right', 'scribbler' ),
		'id'            => 'footer-3',
		'description'   => 'Footer - Right',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'scribbler_widgets_init' );



/**
 * Enqueue scripts and styles.
 */
function scribbler_scripts() {

	// All of the following commented scripts are now being loaded
	// through concat in Grunt.
	// 
	// commenting out loading of style.css for now
	// wp_enqueue_style( 'scribbler-style', get_stylesheet_uri() );

	// todo: enqueue through concatenated script
	//wp_enqueue_script( 'scribbler-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	// todo: enqueue through concatenated script
	//wp_enqueue_script( 'scribbler-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'scribbler_scripts' );

/**
 * Implement the Custom Header feature.
 */
require SCRIBBLER_INC . '/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require SCRIBBLER_INC . '/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require SCRIBBLER_INC . '/extras.php';

/**
 * Customizer additions.
 */
require SCRIBBLER_INC . '/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require SCRIBBLER_INC . '/jetpack.php';

/**
 * Load Bootstrap compatibility file.
 */
require SCRIBBLER_INC . '/functions/bootstrap-tools.php';

// Run the setup functions
IvyCat\Scribbler\Core\setup();

// Hide Admin Bar
//add_filter( 'show_admin_bar', '__return_false' );